﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnerBehaviour : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ballPrefab;
    public GameObject ballCopy;
    [SerializeField] private int maxBalls = 10;
    private int ballCount = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    float elapsed = 0f;
    void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= 0.25f)
        {
            elapsed = elapsed % 0.25f;
            if(ballCount < maxBalls)
                SpawnBall();
        }
    }
    void SpawnBall()
    {
        Vector3 randomLocation = new Vector3(Random.Range(-5,5), Random.Range(-3, 3), Random.Range(-10, -5));
        ballCopy = Instantiate(ballPrefab, randomLocation, transform.rotation);
        ballCopy.name = "Ball" + ballCount;
        ballCount++;
    }

    public int GetBallCount()
    {
        return ballCount;
    }

    public int GetBallMax()
    {
        return maxBalls;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
    [SerializeField] private GameObject spawner;
    [SerializeField] private Text ballCounter;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ballCounter.text = "Balls created: " + spawner.GetComponent<SpawnerBehaviour>().GetBallCount().ToString();
    }
}

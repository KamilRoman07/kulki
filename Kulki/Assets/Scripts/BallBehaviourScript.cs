﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]private int ballMass = 1;
    public GameObject ballPrefab;
    public GameObject ballCopy;
    public float range = 1000f;
    private int weight;
    private bool rolled = false;
    [SerializeField] private GameObject spawner;
    [SerializeField] private float airResistanceForce = 1.5f;

    void Start()
    {
        spawner = GameObject.FindWithTag("GameController");
        Invoke("StartColliding", 0.5f);
    }

    void FixedUpdate()
    {
        ApplyGravity();
        ApplyAirResistance();
        if (this.ballMass >= 50)
            Explode();
    }

    private void ApplyAirResistance()
    {
        Rigidbody rb = this.GetComponent<Rigidbody>();
        float x, y, z;
        Vector3 rbVelocity = rb.velocity;
        if (rbVelocity.x > 0)
            x = -airResistanceForce;
        else
            x = airResistanceForce;

        if (rbVelocity.y > 0)
            y = -airResistanceForce;
        else
            y = airResistanceForce;

        if (rbVelocity.z > 0)
            z = -airResistanceForce;
        else
            z = airResistanceForce;

        rb.AddForce(x, y, z);
    }

    private void ApplyGravity()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, range);
        List<Rigidbody> rbs = new List<Rigidbody>();

        foreach (Collider c in cols)
        {
            Rigidbody rb = c.attachedRigidbody;
            if (rb != null && rb != GetComponent<Rigidbody>() && !rbs.Contains(rb))
            {
                rbs.Add(rb);
                Vector3 offset;
                if (spawner.GetComponent<SpawnerBehaviour>().GetBallCount() < spawner.GetComponent<SpawnerBehaviour>().GetBallMax())
                    offset = transform.position - c.transform.position;
                else
                    offset = c.transform.position - transform.position;
                rb.AddForce(offset / offset.sqrMagnitude * this.ballMass);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (spawner.GetComponent<SpawnerBehaviour>().GetBallCount() < spawner.GetComponent<SpawnerBehaviour>().GetBallMax())
        {
            this.GetComponent<Collider>().enabled = false;
            Vector3 position = new Vector3(collision.contacts[0].point.x, collision.contacts[0].point.y, collision.contacts[0].point.z);
            BallBehaviourScript collidedBallScript = collision.gameObject.GetComponent(typeof(BallBehaviourScript)) as BallBehaviourScript;

            weight = Random.Range(0, 10000);
            rolled = true;

            if (!collidedBallScript.GetRolled())
            {
                float area = Mathf.Pow(this.transform.localScale.x, 2f) * 4 * Mathf.PI;
                area += Mathf.Pow(collision.gameObject.transform.localScale.x, 2f) * 4 * Mathf.PI;
                float r = Mathf.Sqrt(area / 4 / Mathf.PI);
                ballCopy = Instantiate(ballPrefab, position, transform.rotation);
                ballCopy.transform.localScale = new Vector3(r,r,r);
                ballCopy.GetComponent<BallBehaviourScript>().SetBallMass(this.ballMass + collidedBallScript.GetBallMass());
                ballCopy.GetComponent<Collider>().enabled = true;
            }
            Destroy(collision.gameObject);
        }
    }

    private void StartColliding()
    {
        this.GetComponent<Collider>().enabled = true;
    }

    private void Explode()
    {
        int count = this.ballMass;
        this.ballMass = 0;
        gameObject.transform.localScale = new Vector3(1, 1, 1);
        for(int i = 0; i < count; i++)
        {
            ballCopy = Instantiate(ballPrefab, transform.position, transform.rotation);
            ballCopy.GetComponent<Collider>().enabled = false;
            ballCopy.GetComponent<BallBehaviourScript>().SetBallMass(1);
            Rigidbody rb = ballCopy.GetComponent<Rigidbody>();
            Vector3 force = new Vector3(Random.Range(-1000,1000), Random.Range(-1000, 1000), Random.Range(-1000, 1000));
            rb.AddForce(force);
        }
        Destroy(gameObject);
    }

    public void SetBallMass(int mass)
    {
        this.ballMass = mass;
    }

    public int GetBallMass()
    {
        return this.ballMass;
    }

    public int GetWeight()
    {
        return weight;
    }

    public bool GetRolled()
    {
        return rolled;
    }

}
